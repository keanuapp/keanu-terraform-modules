variable "cloudflare_dns_api_token" {
  type = string
}

variable "cloudflare_zone_api_token" {
  type = string
}

variable "use_production" {
  type    = string
  default = "0"
}

variable "force_renewal" {
  type    = string
  default = "0"
}

variable "min_days_remaining" {
  type    = string
  default = "30"
}

variable "acme_account_key_pem" {
  type = string
}

variable "tls_private_key_algo" {
  type = string
}

variable "tls_private_key" {
  type = string
}

variable "dns_names" {
  type = list(string)
}

variable "common_name" {
  type = string
}

### label
variable "delimiter" {
  type        = string
  default     = "-"
  description = "Delimiter to be used between `namespace`, `environment`, `name`, and `attributes`"
}

variable "attributes" {
  type        = list(string)
  default     = []
  description = "Additional attributes (e.g. `1`)"
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "Additional tags (e.g. map(`BusinessUnit`,`XYZ`)"
}

variable "namespace" {
  type        = string
  description = "Namespace (e.g. `keanu`)"
}

variable "environment" {
  type        = string
  description = "environment (e.g. `prod`, `dev`, `staging`, `infra`)"
}

variable "name" {
  type        = string
  description = "Name  (e.g. `app` or `cluster`)"
}

variable "acme_account_production_server_url" {
  type    = string
  default = "https://acme-v02.api.letsencrypt.org/directory"
}
