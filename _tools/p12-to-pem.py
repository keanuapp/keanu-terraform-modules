import json
import sys
import os
import getpass


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


try:
    from OpenSSL import crypto
except ModuleNotFoundError:
    eprint("pyopenssl not found. Try: apt install python3-openssl")
    sys.exit(3)


def usage():
    print("usage: {} path/to/.p12".format(sys.argv[0]))
    sys.exit(1)


def p12_to_pem(p12path):
    if not os.path.exists(path):
        eprint("Supplied p12 doesn't exist: {}".format(path))
        sys.exit(2)

    if not sys.stdin.isatty():
        password = sys.stdin.read()
    else:
        password = getpass.getpass()

    p12_bytes = open(path, 'rb').read()
    try:
        p12 = crypto.load_pkcs12(p12_bytes, bytes(password.strip(), 'utf-8'))
    except Exception as e:
        eprint("Invalid password or corrupted p12 file.")
        sys.exit(4)

    pkey = crypto.dump_privatekey(crypto.FILETYPE_PEM, p12.get_privatekey())
    cert = crypto.dump_certificate(crypto.FILETYPE_PEM, p12.get_certificate())
    pem = "{}{}".format(cert.decode('ascii'), pkey.decode('ascii').strip())
    return pem


if len(sys.argv) != 2 or sys.argv[1] in ["-h", "--help"]:
    usage()
path = sys.argv[1]
pem = p12_to_pem(path)
print(pem)
