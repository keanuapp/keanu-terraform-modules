output "front_domain" {
  value = {for f in var.front_names :
           f => aws_cloudfront_distribution.front[f].domain_name}
}
