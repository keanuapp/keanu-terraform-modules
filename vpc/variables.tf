variable "region" {
  type = string
}

variable "cidr_block" {
  type    = string
  default = "10.55.0.0/16"
}

variable "subnet_public_1" {
  type    = string
  default = "10.55.0.0/24"
}

variable "subnet_public_2" {
  type    = string
  default = "10.55.1.0/24"
}

variable "subnet_private_1" {
  type    = string
  default = "10.55.2.0/24"
}

variable "subnet_private_2" {
  type    = string
  default = "10.55.3.0/24"
}

variable "subnet_rds_a_1" {
  type    = string
  default = "10.55.4.0/28"
}

variable "subnet_rds_a_2" {
  type    = string
  default = "10.55.4.16/28"
}

variable "availability_zone_1" {
  type = string
}

variable "availability_zone_2" {
  type = string
}

variable "delimiter" {
  type        = string
  default     = "-"
  description = "Delimiter to be used between `namespace`, `environment`, `name`, and `attributes`"
}

variable "attributes" {
  type        = list(string)
  default     = []
  description = "Additional attributes (e.g. `1`)"
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "Additional tags (e.g. map(`BusinessUnit`,`XYZ`)"
}

variable "namespace" {
  type        = string
  description = "Namespace (e.g. `keanu`)"
}

variable "environment" {
  type        = string
  description = "environment (e.g. `prod`, `dev`, `staging`, `infra`)"
}

variable "name" {
  type        = string
  description = "Name  (e.g. `app` or `cluster`)"
}
