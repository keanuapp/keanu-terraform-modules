module "label" {
  source             = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.16.0"
  namespace          = var.namespace
  environment        = var.environment
  name               = var.name
  delimiter          = var.delimiter
  attributes         = ["ssm-sessions"]
  tags               = var.tags
  additional_tag_map = var.additional_tag_map
  label_order        = var.label_order
}

resource "aws_s3_bucket" "default" {
  bucket        = module.label.id
  acl           = "private"
  region        = var.region
  force_destroy = var.force_destroy

  versioning {
    enabled = false
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  tags = module.label.tags
}

resource "aws_ssm_document" "session_manager_prefs" {
  name            = "SSM-SessionManagerRunShell"
  document_type   = "Session"
  document_format = "JSON"

  content = <<DOC
{
    "schemaVersion": "1.0",
    "description": "Document to hold regional settings for Session Manager",
    "sessionType": "Standard_Stream",
    "inputs": {
        "s3BucketName": "${aws_s3_bucket.default.id}",
        "s3KeyPrefix": "${var.s3_key_prefix}",
        "s3EncryptionEnabled": ${var.s3_encryption_enabled ? "true" : "false"},
        "cloudWatchLogGroupName": "${var.cloudwatch_log_group_name}",
        "cloudWatchEncryptionEnabled": ${var.cloudwatch_encryption_enabled ? "true" : "false"}
    }
}
DOC

}
