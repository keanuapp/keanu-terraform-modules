import json
import sys
import os
import getpass


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


try:
    from OpenSSL import crypto
except ModuleNotFoundError:
    eprint("pyopenssl not found. Try: apt install python3-openssl")
    sys.exit(3)


def usage():
    print("usage: {} path/to/.p12".format(sys.argv[0]))
    sys.exit(1)


def export_p12(filename, cert, key, password):
    pkcs = crypto.PKCS12()
    pkcs.set_privatekey(key)
    pkcs.set_certificate(cert)
    with open(filename, 'wb') as file:
        file.write(pkcs.export(passphrase=password))

def load_p12(p12path):
    if not os.path.exists(path):
        eprint("Supplied p12 doesn't exist: {}".format(path))
        sys.exit(2)

    password = bytes(getpass.getpass(prompt="Old password: ").strip(), 'utf-8')

    p12_bytes = open(path, 'rb').read()
    try:
        p12 = crypto.load_pkcs12(p12_bytes, password)
    except Exception as e:
        eprint("Invalid password or corrupted p12 file.")
        sys.exit(4)

    pkey = p12.get_privatekey()
    cert = p12.get_certificate()
    return cert,pkey


if len(sys.argv) != 2 or sys.argv[1] in ["-h", "--help"]:
    usage()
path = sys.argv[1]
key, cert = load_p12(path)

new_pass = bytes(getpass.getpass(prompt="New password: ").strip(), 'utf-8')
export_p12(path, key, cert, new_pass)
